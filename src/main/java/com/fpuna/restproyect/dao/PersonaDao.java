/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpuna.restproyect.dao;

import com.fpuna.restproyect.entidades.Persona;
import java.util.List;
 

 
public interface PersonaDao {
 
    void guardar(Persona persona);
     
    List<Persona> buscarTodos();
     
    void borrarPorID(String ssn);
     
    Persona buscarPorId(String ssn);
     
    void actualizarPersona(Persona persona);
}