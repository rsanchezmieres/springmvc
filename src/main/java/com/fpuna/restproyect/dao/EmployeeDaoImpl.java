/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpuna.restproyect.dao;

import com.fpuna.restproyect.entidades.Employee2;
import java.util.List;
 
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
 

 
@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractDao implements EmployeeDao{
 
    public void saveEmployee2(Employee2 employee) {
        persist(employee);
    }
 
    @SuppressWarnings("unchecked")
    public List<Employee2> findAllEmployees() {
        Criteria criteria = getSession().createCriteria(Employee2.class);
        return (List<Employee2>) criteria.list();
    }
 
    public void deleteEmployeeBySsn(String ssn) {
        Query query = getSession().createSQLQuery("delete from Employee2 where ssn = :ssn");
        query.setString("ssn", ssn);
        query.executeUpdate();
    }
 
     
    public Employee2 findBySsn(String ssn){
        Criteria criteria = getSession().createCriteria(Employee2.class);
        criteria.add(Restrictions.eq("ssn",ssn));
        return (Employee2) criteria.uniqueResult();
    }
     
    public void updateEmployee(Employee2 employee){
        getSession().update(employee);
    }

    @Override
    public void saveEmployee(Employee2 employee) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
}
