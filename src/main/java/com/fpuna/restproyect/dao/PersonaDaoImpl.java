/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpuna.restproyect.dao;

import com.fpuna.restproyect.entidades.Employee2;
import com.fpuna.restproyect.entidades.Persona;
import java.util.List;
 
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
 

 
@Repository("personaDao")
public class PersonaDaoImpl extends AbstractDao implements PersonaDao{
 
    public void guardar(Persona persona) {
        persist(persona);
    }
 
    @SuppressWarnings("unchecked")
    public List<Persona> buscarTodos() {
        Criteria criteria = getSession().createCriteria(Persona.class);
        return (List<Persona>) criteria.list();
    }
 
    public void borrarPorID(String ssn) {
        Query query = getSession().createSQLQuery("delete from Persona where id = :id");
        query.setString("id", ssn);
        query.executeUpdate();
    }
 
     
    public Persona buscarPorId(String ssn){
        Criteria criteria = getSession().createCriteria(Persona.class);
        criteria.add(Restrictions.eq("id",Integer.parseInt(ssn)));
        return (Persona) criteria.uniqueResult();
    }
     
    public void actualizarPersona(Persona persona){
        getSession().update(persona);
    }

   

     
}
