/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpuna.restproyect.dao;

import com.fpuna.restproyect.entidades.Employee2;
import java.util.List;
 

 
public interface EmployeeDao {
 
    void saveEmployee(Employee2 employee);
     
    List<Employee2> findAllEmployees();
     
    void deleteEmployeeBySsn(String ssn);
     
    Employee2 findBySsn(String ssn);
     
    void updateEmployee(Employee2 employee);
}