package com.fpuna.restproyect.service;

 
import com.fpuna.restproyect.dao.EmployeeDao;
import com.fpuna.restproyect.dao.PersonaDao;
import com.fpuna.restproyect.entidades.Persona;
import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
 
@Service("personaService")
@Transactional
public class PersonaServiceImpl implements PersonaService{
 
    @Autowired
    private PersonaDao dao;
     
    public void guardar(Persona persona) {
        dao.guardar(persona);
    }
 
    public List<Persona> buscarTodos() {
        return dao.buscarTodos();
    }
 
    public void borrarPorId(String ssn) {
        dao.borrarPorID(ssn);
    }
 
    public Persona buscarPorId(String ssn) {
        return dao.buscarPorId(ssn);
    }
 
    public void actualizarPersona(Persona persona){
        dao.actualizarPersona(persona);
    }
}
