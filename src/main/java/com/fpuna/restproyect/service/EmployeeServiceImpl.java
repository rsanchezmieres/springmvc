package com.fpuna.restproyect.service;

 
import com.fpuna.restproyect.dao.EmployeeDao;
import com.fpuna.restproyect.entidades.Employee2;
import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
 
@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
 
    @Autowired
    private EmployeeDao dao;
     
    public void saveEmployee(Employee2 employee) {
        dao.saveEmployee(employee);
    }
 
    public List<Employee2> findAllEmployees() {
        return dao.findAllEmployees();
    }
 
    public void deleteEmployeeBySsn(String ssn) {
        dao.deleteEmployeeBySsn(ssn);
    }
 
    public Employee2 findBySsn(String ssn) {
        return dao.findBySsn(ssn);
    }
 
    public void updateEmployee(Employee2 employee){
        dao.updateEmployee(employee);
    }
}
