package com.fpuna.restproyect.contoller;

import com.fpuna.restproyect.entidades.Employee2;
import com.fpuna.restproyect.entidades.Persona;
import com.fpuna.restproyect.service.EmployeeService;
import com.fpuna.restproyect.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AppController {
    
    @Autowired
    private PersonaService ps;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    String home(ModelMap model) {
        
        return "rest";
    }

    @RequestMapping(value = "/empleado/{ssn}", method = RequestMethod.GET)
    public @ResponseBody Persona getPerson(@PathVariable String ssn) {
        Persona per = ps.buscarPorId(ssn);
        if(per == null){
           per = new Persona();
           per.setNombre("NO EXISTE");
           per.setId(2);
        }
        return per;
    }
    @RequestMapping(value = "/persona/guardar/{ssn}", method = RequestMethod.GET)
    public @ResponseBody String buscar(@PathVariable String ssn) {
        
        Persona p = new Persona();
        p.setNombre("ROQUE");
        p.setApellido("SANCHEZ");
        p.setCedula("4283837");
        p.setNacionalidad("PARAGUAY");
        
        ps.guardar(p);
        
        return "exito";
    }
}
